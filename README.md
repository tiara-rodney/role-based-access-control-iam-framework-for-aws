# iam-rbac.aws

- Segregation of Duty

It is also helpful in getting familiar with the IAM concept of AWS, since 

- What role did the individual hold when performing a certain action?
- Is the individual aware of the consequences his action will cause?
- Is the individual permitted to

## Entities

### ArchitectRole
The architect role represents individuals, who are actively manipulating AWS resources. This usually includes developers, engineers & administrators.

### AuditorRole
The auditor role represents individuals, who are required to monitor AWS activities and states, but are not to interfere or manipulate AWS ressources.

Use-Cases:
    - Checking for compliance (security auditing)
    - Monitoring AWS resources 

### SafeArchitectRole
In contrast to the architect role, the safe architect role is not able to deploy any ressources with long-term contractual liabilities (e.g. EC2 reserved instances).

### Governor Role
The governor role replaces the root login to an account. A governor has full access to the aws account.

### IAMAdministratorRole
Responsible for adding indivduals permissions to switch into a certain role. An IAMAdministrator however is not able to escalate a user to switch into the governor role. Only a governor is able to do that.

### OrganizationAdministratorRole


All IAM Users MUST BE a member of the IAM group ``

## Assurance Testing


### Plain Access Activity
applies to authenticated `IAM User` who has not assumed any role 

- `IAM User` members of `IAM Group` [$Prefix]SelfManagedUsers can update their own IAM user profile
- `IAM User` members of any prefixed/governed `IAM Group` can only assume the according `IAM Role` linked to the `IAM Group` (except for [$Prefix]SelfManagedUsers)

### IAM Administration Activity

- `IAM Role` [$Prefix]IAMAdministrator and [$Prefix]Governor are the only IAM entities able to perform modifying actions on IAM ressources.
- `IAM Role` [$Prefix]IAMAdministrator cannot add `IAM User` to `IAM Group` [$Prefix]Governors
- `IAM Role` [$Prefix]IAMAdministrator cannot apply `IAM Policy` to any `IAM Group` if `IAM Policy` permission boundary [$Prefix]GlobalPermissionsBoundary is not attached to the `IAM Group`
- `IAM Role` [$Prefix]IAMAdministrator cannot attach managed `ÌAM Policy` to any `IAM Group`
- `IAM Role` [$Prefix]IAMAdministrator can attach inline `IAM Policy` to any `IAM Group`
- `IAM Role` [$Prefix]IAMAdministrator can create and modify `IAM User`
- `IAM Role` [$Prefix]IAMAdministrator cannot attach any type of `IAM Policy` to an `IAM User`
- `IAM Role` [$Prefix]IAMAdministrator can add `IAM User` to `IAM Group`
 

###MFA-dependent Activity
only applies if `EnforceMFA` flag is set to `true`

- `IAM User` with authenticated MFA can update his own IAM user profile
- `IAM User` without authenticated MFA cannot update his own IAM user profile
- `IAM User` with & without authenticated MFA can add a new virtual MFA device to his IAM user
- `IAM User` with authenticated MFA can switch into role
- `IAM User` without authenticated MFA cannot switch into role

